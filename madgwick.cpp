#include <math.h>

#include "madgwick.h"
#include "basic_linalg.h"

#define DEG_RAD 57.295780181884765625f   			// Amount of degrees per radian
#define RAD_DEG 0.01745329238474369049072265625f	// Amount of radians per degree

#define _W 	0
#define _X	1
#define _Y 	2
#define _Z	3

#define X	0
#define Y 	1
#define Z	2

// Constructor
Madgwick::Madgwick(float sample_time, float beta) {

	beta_ = beta;

	q_[_W] = 1.0f; // Initialize upright
	q_[_X] = 0.0f;
	q_[_Y] = 0.0f;
	q_[_Z] = 0.0f;

	sample_time_ = sample_time;

	angles_computed_ = false;
}

// update
void Madgwick::update(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz) {

	float gyr[3] = {gx, gy, gz};
	float acc[3] = {ax, ay, az};
	float mag[3] = {mx, my, mz};

	update(gyr, acc, mag);
}

void Madgwick::update(const float gyr_deg[3], const float acc[3], const float mag[3]) {

	// Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
	if (normSquared(mag) < 1.0e-5f) {
		updateIMU(gyr_deg, acc);
		return;
	}

	// Convert gyroscope degrees/sec to radians/sec
	float gyr[3];
	scalarMultiply(gyr_deg, RAD_DEG, gyr);

	// Rate of change of quaternion from gyroscope
	float qDot1 = 0.5f * (-q_[_X] * gyr[X] - q_[_Y] * gyr[Y] - q_[_Z] * gyr[Z]);
	float qDot2 = 0.5f * (q_[_W] * gyr[X] + q_[_Y] * gyr[Z] - q_[_Z] * gyr[Y]);
	float qDot3 = 0.5f * (q_[_W] * gyr[Y] - q_[_X] * gyr[Z] + q_[_Z] * gyr[X]);
	float qDot4 = 0.5f * (q_[_W] * gyr[Z] + q_[_X] * gyr[Y] - q_[_Y] * gyr[X]);

	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if (normSquared(acc) > 1.0e-5f) {

		float acc_norm[3], mag_norm[3];
		normalize(acc_norm); // Normalise accelerometer measurement
		normalize(mag_norm); // Normalise magnetometer measurement

		// Auxiliary variables to avoid repeated arithmetic
		float _2q0mx = 2.0f * q_[_W] * mag_norm[X];
		float _2q0my = 2.0f * q_[_W] * mag_norm[Y];
		float _2q0mz = 2.0f * q_[_W] * mag_norm[Z];
		float _2q1mx = 2.0f * q_[_X] * mag_norm[X];
		float _2q0 = 2.0f * q_[_W];
		float _2q1 = 2.0f * q_[_X];
		float _2q2 = 2.0f * q_[_Y];
		float _2q3 = 2.0f * q_[_Z];
		float _2q0q2 = 2.0f * q_[_W] * q_[_Y];
		float _2q2q3 = 2.0f * q_[_Y] * q_[_Z];
		float q0q0 = q_[_W] * q_[_W];
		float q0q1 = q_[_W] * q_[_X];
		float q0q2 = q_[_W] * q_[_Y];
		float q0q3 = q_[_W] * q_[_Z];
		float q1q1 = q_[_X] * q_[_X];
		float q1q2 = q_[_X] * q_[_Y];
		float q1q3 = q_[_X] * q_[_Z];
		float q2q2 = q_[_Y] * q_[_Y];
		float q2q3 = q_[_Y] * q_[_Z];
		float q3q3 = q_[_Z] * q_[_Z];

		// Reference direction of Earth's magnetic field
		float hx = mag_norm[X] * q0q0 - _2q0my * q_[_Z] + _2q0mz * q_[_Y] + mag_norm[X] * q1q1 + _2q1 * mag_norm[Y] * q_[_Y] + _2q1 * mag_norm[Z] * q_[_Z] - mag_norm[X] * q2q2 - mag_norm[X] * q3q3;
		float hy = _2q0mx * q_[_Z] + mag_norm[Y] * q0q0 - _2q0mz * q_[_X] + _2q1mx * q_[_Y] - mag_norm[Y] * q1q1 + mag_norm[Y] * q2q2 + _2q2 * mag_norm[Z] * q_[_Z] - mag_norm[Y] * q3q3;
		float _2bx = sqrtf(hx * hx + hy * hy);
		float _2bz = -_2q0mx * q_[_Y] + _2q0my * q_[_X] + mag_norm[Z] * q0q0 + _2q1mx * q_[_Z] - mag_norm[Z] * q1q1 + _2q2 * mag_norm[Y] * q_[_Z] - mag_norm[Z] * q2q2 + mag_norm[Z] * q3q3;
		float _4bx = 2.0f * _2bx;
		float _4bz = 2.0f * _2bz;

		// Gradient decent algorithm corrective step
		float s[4];
		s[0] = -_2q2 * (2.0f * q1q3 - _2q0q2 - acc_norm[X]) + _2q1 * (2.0f * q0q1 + _2q2q3 - acc_norm[Y]) - _2bz * q_[_Y] * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mag_norm[X]) + (-_2bx * q_[_Z] + _2bz * q_[_X]) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - mag_norm[Y]) + _2bx * q_[_Y] * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mag_norm[Z]);
		s[1] = _2q3 * (2.0f * q1q3 - _2q0q2 - acc_norm[X]) + _2q0 * (2.0f * q0q1 + _2q2q3 - acc_norm[Y]) - 4.0f * q_[_X] * (1 - 2.0f * q1q1 - 2.0f * q2q2 - acc_norm[Z]) + _2bz * q_[_Z] * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mag_norm[X]) + (_2bx * q_[_Y] + _2bz * q_[_W]) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - mag_norm[Y]) + (_2bx * q_[_Z] - _4bz * q_[_X]) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mag_norm[Z]);
		s[2] = -_2q0 * (2.0f * q1q3 - _2q0q2 - acc_norm[X]) + _2q3 * (2.0f * q0q1 + _2q2q3 - acc_norm[Y]) - 4.0f * q_[_Y] * (1 - 2.0f * q1q1 - 2.0f * q2q2 - acc_norm[Z]) + (-_4bx * q_[_Y] - _2bz * q_[_W]) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mag_norm[X]) + (_2bx * q_[_X] + _2bz * q_[_Z]) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - mag_norm[Y]) + (_2bx * q_[_W] - _4bz * q_[_Y]) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mag_norm[Z]);
		s[3] = _2q1 * (2.0f * q1q3 - _2q0q2 - acc_norm[X]) + _2q2 * (2.0f * q0q1 + _2q2q3 - acc_norm[Y]) + (-_4bx * q_[_Z] + _2bz * q_[_X]) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mag_norm[X]) + (-_2bx * q_[_W] + _2bz * q_[_Y]) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - mag_norm[Y]) + _2bx * q_[_X] * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mag_norm[Z]);
		
		normalize(s, 4); // Normalise step magnitude

		// Apply feedback step
		qDot1 -= beta_ * s[0];
		qDot2 -= beta_ * s[1];
		qDot3 -= beta_ * s[2];
		qDot4 -= beta_ * s[3];
	}

	// Integrate rate of change of quaternion to yield quaternion
	q_[_W] += qDot1 * sample_time_;
	q_[_X] += qDot2 * sample_time_;
	q_[_Y] += qDot3 * sample_time_;
	q_[_Z] += qDot4 * sample_time_;

	// Normalise quaternion
    normalize(q_, 4);

	angles_computed_ = false;
}

// updateIMU
void Madgwick::updateIMU(float gx, float gy, float gz, float ax, float ay, float az) {

	float gyr[3] = {gx, gy, gz};
	float acc[3] = {ax, ay, az};

	updateIMU(gyr, acc);
}

void Madgwick::updateIMU(const float gyr_deg[3], const float acc[3]) {

	// Convert gyroscope degrees/sec to radians/sec
	float gyr[3];
	scalarMultiply(gyr_deg, RAD_DEG, gyr);

	// Rate of change of quaternion from gyroscope
	float qDot1 = 0.5f * (-q_[_X] * gyr[X] - q_[_Y] * gyr[Y] - q_[_Z] * gyr[Z]);
	float qDot2 = 0.5f * (q_[_W] * gyr[X] + q_[_Y] * gyr[Z] - q_[_Z] * gyr[Y]);
	float qDot3 = 0.5f * (q_[_W] * gyr[Y] - q_[_X] * gyr[Z] + q_[_Z] * gyr[X]);
	float qDot4 = 0.5f * (q_[_W] * gyr[Z] + q_[_X] * gyr[Y] - q_[_Y] * gyr[X]);

	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if (normSquared(acc) > 1.0e-5f) {

		float acc_norm[3];
		normalize(acc, acc_norm); // Normalise accelerometer measurement

		// Auxiliary variables to avoid repeated arithmetic
		float _2q0 = 2.0f * q_[_W];
		float _2q1 = 2.0f * q_[_X];
		float _2q2 = 2.0f * q_[_Y];
		float _2q3 = 2.0f * q_[_Z];
		float _4q0 = 4.0f * q_[_W];
		float _4q1 = 4.0f * q_[_X];
		float _4q2 = 4.0f * q_[_Y];
		float _8q1 = 8.0f * q_[_X];
		float _8q2 = 8.0f * q_[_Y];
		float q0q0 = q_[_W] * q_[_W];
		float q1q1 = q_[_X] * q_[_X];
		float q2q2 = q_[_Y] * q_[_Y];
		float q3q3 = q_[_Z] * q_[_Z];

		// Gradient decent algorithm corrective step
		float s[4];
		s[0] = _4q0 * q2q2 + _2q2 * acc_norm[X] + _4q0 * q1q1 - _2q1 * acc_norm[Y];
		s[1] = _4q1 * q3q3 - _2q3 * acc_norm[X] + 4.0f * q0q0 * q_[_X] - _2q0 * acc_norm[Y] - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * acc_norm[Z];
		s[2] = 4.0f * q0q0 * q_[_Y] + _2q0 * acc_norm[X] + _4q2 * q3q3 - _2q3 * acc_norm[Y] - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * acc_norm[Z];
		s[3] = 4.0f * q1q1 * q_[_Z] - _2q1 * acc_norm[X] + 4.0f * q2q2 * q_[_Z] - _2q2 * acc_norm[Y];

		normalize(s, 4);

		// Apply feedback step
		qDot1 -= beta_ * s[0];
		qDot2 -= beta_ * s[1];
		qDot3 -= beta_ * s[2];
		qDot4 -= beta_ * s[3];
	}

	// Integrate rate of change of quaternion to yield quaternion
	q_[_W] += qDot1 * sample_time_;
	q_[_X] += qDot2 * sample_time_;
	q_[_Y] += qDot3 * sample_time_;
	q_[_Z] += qDot4 * sample_time_;

	// Normalise quaternion
    normalize(q_, 4);

	angles_computed_ = false;	
}

// quaternionInit

void Madgwick::quaternionInit(float ax, float ay, float az, float mx, float my, float mz) {

	float acc[3] = {ax, ay, az};
	float mag[3] = {mx, my, mz};

	quaternionInit(acc, mag);
}

void Madgwick::quaternionInit(const float acc[3], const float mag[3]) {

	// H = cross(a, m)
	float H[3];
	cross(acc, mag, H);
	normalize(H);

	float acc_norm[3];
	normalize(acc, acc_norm);

	// M = cross(a, H)
	float M[3];
	cross(acc_norm, H, M);

	// [ H, M, a] now makes up a 3x3 Direction Cosine Matrix 'R' (for 'ENU' convention)

	q_[_W] = 0.5f * sqrt(1.0f + H[X] + mag[Y] + acc_norm[Z]); // 0.5 * sqrt(1 + trace(R))
	q_[_X] = 0.25f * (acc_norm[Y] - mag[Z]) / q_[_W];
	q_[_Y] = 0.25f * (H[Z] - acc_norm[X]) / q_[_W];
	q_[_Z] = 0.25f * (mag[X] - H[Y]) / q_[_W];

	normalize(q_, 4);
}

// quaternionInitIMU
// See: https://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another
// Simply get the shortest arc between the expected gravity, [0, 0, 1.0] and the 
// measured acceleration.
void Madgwick::quaternionInitIMU(float ax, float ay, float az) {

	float acc[3] = {ax, ay, az};

	quaternionInitIMU(acc);
}

void Madgwick::quaternionInitIMU(const float acc[3]) {

    // q.xyz = cross(a, g), g = [0, 0, 1]
    q_[_X] = acc[Y];
    q_[_Y] = -acc[X];
    q_[_Z] = 0.0f; 

    float a_norm_sq = normSquared(acc);

    q_[_W] = sqrt(a_norm_sq + 1.0f) + acc[Z]; // sqrt( ||v1||^2 + ||v2||^2) + dot(a, g)

    normalize(q_, 4);
    
    angles_computed_ = false;
}

// getQuaternion
void Madgwick::getQuaternion(float quat[4]) const {
    quat[0] = q_[_W];
    quat[1] = q_[_X];
    quat[2] = q_[_Y];
    quat[3] = q_[_Z];
}

const float* Madgwick::getQuaternion() const {
    return q_;
}

float* Madgwick::quaternion() {
	return q_;
}

// setQuaternion
void Madgwick::setQuaternion(float q0, float q1, float q2, float q3) {
    q_[_W] = q0;
    q_[_X] = q1;
    q_[_Y] = q2;
    q_[_Z] = q3;

    angles_computed_ = false;
}
void Madgwick::setQuaternion(const float quat[4]) {
    
    setQuaternion(quat[0], quat[1], quat[2], quat[3]);
}

// Get Euler angles
float Madgwick::getRoll() {
    if (!angles_computed_) computeEulerAngles();
    return roll_ * DEG_RAD;
}
float Madgwick::getPitch() {
    if (!angles_computed_) computeEulerAngles();
    return pitch_ * DEG_RAD;
}
float Madgwick::getYaw() {
    if (!angles_computed_) computeEulerAngles();
    return yaw_ * DEG_RAD + 180.0f;
}
float Madgwick::getRollRadians() {
    if (!angles_computed_) computeEulerAngles();
    return roll_;
}
float Madgwick::getPitchRadians() {
    if (!angles_computed_) computeEulerAngles();
    return pitch_;
}
float Madgwick::getYawRadians() {
    if (!angles_computed_) computeEulerAngles();
    return yaw_;
}

// computeAngles
void Madgwick::computeEulerAngles()
{
	roll_ = atan2f(q_[_W]*q_[_X] + q_[_Y]*q_[_Z], 0.5f - q_[_X]*q_[_X] - q_[_Y]*q_[_Y]);
	pitch_ = asinf(-2.0f * (q_[_X]*q_[_Z] - q_[_W]*q_[_Y]));
	yaw_ = atan2f(q_[_X]*q_[_Y] + q_[_W]*q_[_Z], 0.5f - q_[_Y]*q_[_Y] - q_[_Z]*q_[_Z]);

	angles_computed_ = true;
}
