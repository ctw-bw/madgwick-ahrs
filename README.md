# Madgwick AHRS

This repository contains a library for the Madgwick IMU filter. A Madgwick filter is a type of sensor fusion filter, that combines accelerometer and gyroscope (and, optionally, magnetometer) data into a single 3D orientation.

The Arduino Madgwick class was taken as a base: https://github.com/arduino-libraries/MadgwickAHRS. Since this Arduino library is no longer maintained, this repo was made separately.  
Additionally, algorithms in the source of the Python `ahrs` package was used for additional features: https://github.com/Mayitzin/ahrs

The code was originally written for Arduino, but so far the C++ code is fairly standard and it should be portable to any platform.

## Madgwick conventions

The filter expects measurements in a right-handed handed system, where +z points up (away from gravity).

The quaternion of the filter is the rotation from the IMU frame back to the world frame, i.e. it is the rotation of the measured acceleration into world frame. So `Q * [0 a] * Q'` (with `*` the quaternion product) results in the acceleration in world reference.
