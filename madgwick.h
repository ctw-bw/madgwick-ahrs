//=============================================================================================
// MadgwickAHRS.h
//=============================================================================================
//
// Implementation of Madgwick IMU and AHRS algorithms.
// See: http://www.x-io.co.uk/open-source-imu-and-ahrs-algorithms/
//
// From the x-io website "Open-source resources available on this website are
// provided under the GNU General Public Licence unless an alternative licence
// is provided in source."
//
// Date			Author          Notes
// 29/09/2011	SOH Madgwick    Initial release
// 02/10/2011	SOH Madgwick	Optimized for reduced CPU load
// 19/02/2012	SOH Madgwick	Magnetometer measurement is normalized
//
// Files were copied from https://github.com/arduino-libraries/MadgwickAHRS
// Since that repo is no longer maintained, the files were moved over.
//
//=============================================================================================
#ifndef MadgwickAHRS_h
#define MadgwickAHRS_h
#include <math.h>

#define sample_time_def 0.01f   /// Default sample time (seconds)
#define beta_def        0.05f    /// Default gain (2 * proportional gain)

/**
 * @brief Madgwick AHRS class.
 * 
 * Use an instance to track 3D orientation using a quaternion based on IMU
 * (6 or 9DOF) data.
 * 
 * The filter uses a right-handed coordinate system, with +Z upwards, +X to
 * the right and +Y forwards. (Though the directions X and Y have little
 * consequence.) That means that starting at rest corresponds to a quaternion
 * [1, 0, 0, 0] with an accelerometer readout of [0, 0, 1.0].
 */
class Madgwick {

public:
    /**
     * @brief Construct a new Madgwick object
     * @param sample_time   Time between samples (seconds)
     */
    Madgwick(float sample_time, float beta);

    /**
     * @brief Process new sample.
     * 
     * Gyroscope is expected in degrees per second. Units for the accelerometer and 
     * magnetometer do not matter as they are normalized first.
     */
    //@{
    void update(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
    void update(const float gyr[3], const float acc[3], const float mag[3]);
    void updateIMU(float gx, float gy, float gz, float ax, float ay, float az);
    void updateIMU(const float gyr[3], const float acc[3]);
    //@}

    /**
     * @brief Initialize the quaternion based on acceleration.
     * 
     * This will of course of only work if the IMU is at rest.
     * The heading (yaw) cannot be determined with acceleration only.
     */
    //@{
    void quaternionInit(float ax, float ay, float az, float mx, float my, float mz);
    void quaternionInit(const float acc[3], const float mag[3]);
    void quaternionInitIMU(float ax, float ay, float az);
    void quaternionInitIMU(const float acc[3]);
    //@}

    /**
     * @brief Get the current quaternion
     * 
     * @param[out] quat     Destination
     */
    void getQuaternion(float quat[4]) const;
    const float* getQuaternion() const;

    /**
     * @brief Get non-const quaternion
     */
    float* quaternion();

    /**
     * @brief Set quaternion to a given value
     */
    //@{
    void setQuaternion(float q0, float q1, float q2, float q3);
    void setQuaternion(const float quat[4]);
    //@}

    /**
     * @brief Get Euler angles in degrees.
     */
    //@{
    float getRoll();
    float getPitch();
    float getYaw();
    //@}

    /**
     * @brief Get Euler angles in radians.
     */
    //@{
    float getRollRadians();
    float getPitchRadians() ;
    float getYawRadians();
    //@}

private:

    /**
     * Compute euler angles from current state and buffer results
     */
    void computeEulerAngles();

    float beta_; // Algorithm gain

    // Quaternion of sensor frame relative to auxiliary frame
    // float q0_; // qw
    // float q1_; // qx
    // float q2_; // qy
    // float q3_; // qz

    float q_[4]; // Quaternion [qw, qx, qy, qz]

    float sample_time_;

    float roll_;
    float pitch_;
    float yaw_;

    /** Whether Euler angles have been computed for the current state */
    bool angles_computed_;

};

#endif
