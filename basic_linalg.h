/**
 * @file basic_linalg.h
 * @author Robert Roos
 * @brief Some basic linear algebra, focused on arrays (no vectors or classes)
 * 
 */

#ifndef BASIC_LINALG_H_
#define BASIC_LINALG_H_

#include <stddef.h>

/**
 * @brief Compute inverse square root (fast implementation)
 */
float invSqrt(float x);

/**
 * @brief Get vector norm squared
 * 
 * @param vec 
 * @param n             Number of dimensions
 */
float normSquared(const float* vec, size_t n = 3);

/**
 * @brief Get dot product of two vectors
 * 
 * @param a 
 * @param b 
 * @param n
 * @return float 
 */
float dot(const float a[3], const float b[3], size_t n = 3);

/**
 * @brief Get vector cross product (for 3D vectors)
 * 
 * @param a First vector
 * @param b Second vector
 * @param[out] c    Result
 */
void cross(const float a[3], const float b[3], float c[3]);

/**
 * @brief Perform normalization (in- or out- of place)
 * 
 * @param[in,out] vec 
 * @param n             Number of dimensions
 */
void normalize(float* vec, size_t n = 3);
void normalize(const float* vec, float* dest, size_t n = 3);

/**
 * @brief Multiply a vector with a scalar
 * 
 * @param[in,out] vec 
 * @param scalar 
 * @param n             Number of dimensions
 */
void scalarMultiply(float* vec, const float& scalar, size_t n = 3);
void scalarMultiply(const float* vec, const float& scalar, float* dest, size_t n = 3);

/**
 * @brief Rotate vector by quaternion
 * 
 * @param[in,out] v 
 * @param q
 */
void rotateVectorByQuaternion(const float v[3], const float q[4], float* dest);
void rotateVectorByQuaternion(float v[3], const float q[4]);

/**
 * @brief Multiple two quaternions
 * 
 * @param q1 
 * @param q2 
 * @param[out] q3 
 */
void quaternionMultiply(const float q1[4], const float q2[4], float q3[4]);

/**
 * @brief Get conjugate (inverse) of a quaternion
 * 
 * If the quaternion is already normalized, the conjugate will also be 
 * the quaternion inverse.
 * 
 * @param[in,out] q 
 */
void quaternionConjugate(float q[4]);
void quaternionConjugate(const float q[4], float dest[4]);

/**
 * @brief Get quaternion with the shortest arc between two vectors
 * 
 * @param v1 
 * @param v2 
 * @param q 
 */
void quaternionFromVectors(const float v1[3], const float v2[3], float q[4]);


#endif
